<?php

/**
 * notepad.module
 */

/*****************************************************************************************
 * Drupal Hooks
 ****************************************************************************************/

/**
 * Implementation of hook_help().
 */
function notepad_help($section) {
  switch ($section) {
    case "node/add#notepad":
      return t("Create a new note");
      break;
  }
}

/**
 * Implementation of hook_perm().
 * Valid permissions for this module
 * @return array An array of valid permissions for the onthisdate module
 */
function notepad_perm() {
  return array('can use notepad');
}

/**
 * Implementation of hook_menu().
 */
function notepad_menu($may_cache) {
  $items = array();
  if ($may_cache) {
    $items[] = array(
      'path'     => 'notepad',
      'title'    => t('My Notes'),
      'callback' => 'notepad_summary',
      'callback arguments' => array('user'),
      'access'   => user_access('can use notepad'),
      'type'     => MENU_NORMAL_ITEM,
      'weight'   => 0,
    );
    $items[] = array(
      'path'     => 'notepad/list',
      'title'    => t('My Notes'),
      'access'   => user_access('can use notepad'),
      'type'     => MENU_DEFAULT_LOCAL_TASK,
    );
    $items[] = array(
      'path'     => 'notepad/list/user',
      'title'    => t('My Personal Notes'),
      'callback' => 'notepad_summary',
      'callback arguments' => array('user'),
      'access'   => user_access('can use notepad'),
      'type'     => MENU_LOCAL_TASK,
      'weight'   => 1,
    );
    $items[] = array(
      'path'     => 'notepad/list/content',
      'title'    => t('Content Notes'),
      'callback' => 'notepad_summary',
      'callback arguments' => array('content'),
      'access'   => user_access('can use notepad'),
      'type'     => MENU_LOCAL_TASK,
      'weight'   => 2,
    );
    $items[] = array(
      'path'     => 'notepad/add',
      'title'    => t('Add'),
      'callback' => 'notepad_summary',
      'callback arguments' => array('add'),
      'access'   => user_access('can use notepad'),
      'type'     => MENU_LOCAL_TASK,
      'weight'   => 2,
    );
    $items[] = array(
      'path'        => 'admin/settings/notepad',
      'title'       => t('Notepad'),
      'description' => t('Choose the node types that can have notes.'),
      'callback'    => 'drupal_get_form',
      'callback arguments' => array('notepad_settings'),
      'access'      => user_access('administer content types'),
      'type'        => MENU_NORMAL_ITEM,
    );
  }
  else {
    if (arg(0) == 'node' && is_numeric(arg(1)) && user_access('can use notepad')) {
      $nid = arg(1);
      $sql = 'SELECT type FROM {node} WHERE nid=%d';
      $type = db_result(db_query($sql, $nid));
      $node_types = variable_get('notepad_node_types', array());
      if (isset($node_types[$type])) {
        $items[] = array(
          'path'     => 'node/'. arg(1) .'/notepad',
          'title'    => t('Notepad'),
          'callback' => 'notepad_node',
          'callback arguments' => array($nid),
          'access'   => user_access('can use notepad'),
          'type'     => MENU_LOCAL_TASK,
          'weight'   => 2
        );
        $items[] = array(
          'path'     => 'node/'. arg(1) .'/notepad/list',
          'title'    => t('List'),
          'access'   => user_access('can use notepad'),
          'type'     => MENU_DEFAULT_LOCAL_TASK,
        );
        $items[] = array(
          'path'     => 'node/'. arg(1) .'/notepad/add',
          'title'    => t('Add'),
          'callback' => 'notepad_node',
          'callback arguments' => array($nid, 'add'),
          'access'   => user_access('can use notepad'),
          'type'     => MENU_LOCAL_TASK,
          'weight'   => 2
        );
      }
    }
  }
  return $items;
}

/**
 * Implementation of hook_block().
 *
 * Generate HTML for the products link block
 * @param op the operation from the URL
 * @param delta offset
 * @returns block HTML
 */
function notepad_block($op = 'list', $delta = 0, $edit = array()) {
  global $user;

  switch ($op) {
    case 'list':
      // listing of blocks, such as on the admin/block page
      $block[0]["info"] = t('Notepad');
      return $block;
      break;
      
    case 'view':
      if ($delta == 0) {
        // Generate block info - only show if have perms to use module
        if (user_access('can use notepad')) {
          $result = db_query("SELECT n.nid, n.title, r.body, m.priority FROM {node} n LEFT JOIN {node_revisions} r ON n.vid = r.vid LEFT JOIN {notepad_node_metadata} m ON n.vid = m.vid WHERE n.type = 'notepad' AND n.uid = %d ORDER BY m.priority ASC LIMIT %d", $user->uid, variable_get('number_of_notes_to_show', 10));
          if (db_num_rows($result) > 0) {
            $rows = array();
            while ($row = db_fetch_object($result)) {
              $rows[] = $row;
            }
          }

          // Now define the subject and pass the rows through the themeable helper function
          $block['subject'] = t('My Notes');
          $block['content'] = theme('notepad_block_content', $rows);
          return $block;
        }
      }
      break;
      
    case 'configure':
      if ($delta == 0) {
        $form['number_of_notes_to_show'] = array(
          '#type' => 'textfield',
          '#title' => t('Number of notes to show'),
          '#size' => 5,
          '#description' => t('The number of results showm in the note block.'),
          '#default_value' => variable_get('number_of_notes_to_show', 10)
        );
        return $form;
      }
      break;
      
    case 'save':
      if ($delta == 0) {
        variable_set('number_of_notes_to_show', $edit['number_of_notes_to_show']);
      }
      break;
  }
}
 
/**
 * Implementation of hook_node_info().
 */
function notepad_node_info() {
  return array(
    'notepad' => array(
      'name'        => t('Note'),
      'module'      => 'notepad',
      'body_label'  => t('Note'),
      'description' => t("A node for recording simple notes."),
    )
  );
}

/**
 * Implementation of hook_form().
 */
function notepad_form(&$node) {
  // Title
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => TRUE,
    '#default_value' => $node->title,
    '#weight' => -5,
    '#description' => t('The title of this note.')
  );

  // full description / body
  $form['body_filter']['body'] = array(
    '#type' => 'textarea',
    '#title' => t('Note body'),
    '#default_value' => $node->body,
    '#rows' => 10,
    '#required' => FALSE
  );
                                       
  $form['body_filter']['format'] = filter_form($node->format);

  // Priority drop down box
  $options = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
  $form['priority'] = array(
    '#type'          => 'select',
    '#title'         => t('Priority'),
    '#default_value' => $node->priority,
    '#options'       => $options,
    '#multiple'      => FALSE,
    '#description'   => t('The priority of the note (0 high, 9 low)  - mainly to govern the order in which they are listed.'),
  );
  
  $related_nid = arg(3);
  $related_nid = (is_numeric($related_nid) ? $related_nid : 0);
  $form['related_nid'] = array('#type' => 'hidden', '#value' => $related_nid);
  
  return $form;
}

/**
 * Implementation of hook_nodeapi().
 *
 * "delete": The node is being deleted.
 * "delete revision": The revision of the node is deleted. You can delete data associated with that revision.
 * "insert": The node is being created (inserted in the database).
 * "load": The node is about to be loaded from the database. This hook can be used to load additional data at this time.
 * "prepare": The node is about to be shown on the add/edit form.
 * "search result": The node is displayed as a search result. If you want to display extra information with the result, return it.
 * "print": Prepare a node view for printing. Used for printer-friendly view in book_module
 * "update": The node is being updated.
 * "submit": The node passed validation and will soon be saved. Modules may use this to make changes to the node before it is saved to the database.
 * "update index": The node is being indexed. If you want additional information to be indexed which is not already visible through nodeapi "view", then you should return it here.
 * "validate": The user has just finished editing the node and is trying to preview or submit it. This hook can be used to check the node data. Errors should be set with form_set_error().
 * "view": The node content is being assembled before rendering. The module may add elements $node->content prior to rendering. This hook will be called after hook_view(). The format of 4node->content is the same as used by Forms API.
 * "alter": the $node->content array has been rendered, so the node body or teaser is filtered and now contains HTML. This op should only be used when text substitution, filtering, or other raw text operations are necessary.
 * "rss item": An RSS feed is generated. The module can return properties to be added to the RSS item generated for this node. See comment_nodeapi() and upload_nodeapi() for examples. The $node passed can also be modified to add or remove contents to the feed item.
 *
 */
function notepad_nodeapi(&$node, $op, $teaser, $page) {
  switch ($op) {
    case 'delete revision':
      // Notice that we're matching a single revision based on the node's vid.
      db_query("DELETE FROM {notepad_node_metadata} WHERE vid = %d", $node->vid);
      break;
  }
}

/**
 * Implementation of hook_load().
 */
function notepad_load($node) {
  // Look up the node metadata
  $additions  = db_fetch_object(db_query("SELECT priority FROM {notepad_node_metadata} WHERE vid = %d", $node->vid));
  return $additions;
}

/**
 * Implementation of hook_view().
 */
function notepad_view($node, $teaser = FALSE, $page = FALSE) {
  $node = node_prepare($node, $teaser);
  return $node;
}

/**
 * Implementation of hook_validate().
 */
function notepad_validate(&$node) {
   // Just return true for now
   return TRUE;
}

/**
 * Implementation of hook_insert().
 */
function notepad_insert($node) {
  $personal = ($node->related_nid ? 0 : 1);
  db_query("INSERT INTO {notepad_node_metadata} (vid, nid, priority, personal) VALUES (%d, %d, %d, %d)", $node->vid, $node->nid, $node->priority, $personal);
  if ($node->related_nid) {
    global $user;
    
    $related_node = node_load($node->related_nid);
    db_query("INSERT INTO {notepad_nodes} (nid, node_id, node_type, uid) VALUES (%d, %d, '%s', %d)", $node->related_nid, $node->nid, $related_node->type, $user->uid);
  }
}

/**
 * Implementation of hook_update().
 */
function notepad_update($node) {
  // if this is a new node or we're adding a new revision,
  if ($node->revision) {
    notepad_insert($node);
  }
  else {
    db_query("UPDATE {notepad_node_metadata} SET priority = %d WHERE vid = %d", $node->priority, $node->vid);
  }
}

/**
 * Implementation of hook_delete().
 */
function notepad_delete($node) {
  db_query("DELETE FROM {notepad_node_metadata} WHERE nid = %d", $node->nid);
  db_query("DELETE FROM {notepad_nodes} WHERE nid = %d OR node_id = %d", $node->nid, $node->nid);
}

/**
 * Implementation of hook_access().
 */
function notepad_access($op, $node) {
  global $user;

  switch ($op) {
    case 'view':
      // Only owner can view a note
      if ($user->uid == $node->uid) {
        return TRUE;
      }
      break;
      
    case 'create':
      // Can only create notes if have perm
      return user_access('can use notepad');
      break;
      
    case 'update':
      // Can only update a note if you're the owner
      if ($user->uid == $node->uid) {
        return TRUE;
      }
      break;
      
    case 'delete':
      // Can only delete a note if you're the owner
      if ($user->uid == $node->uid) {
        return TRUE;
      }
      break;
  }
  
  return FALSE;
}

/**
 * Implementation of hook_link().
 */
function notepad_link($type, $node = NULL, $teaser = FALSE) {
  $links = array();
  if (!is_null($node)) {
    $notepad_node_types = variable_get('notepad_node_types', array());
    if (isset($notepad_node_types[$node->type]) && user_access('can use notepad')) {
      $node_types = node_get_types('names');
      $links['notepad'] = array(
        'title' => t('Add Note'),
        'href' => 'node/' . $node->nid . '/notepad/add',
        'attributes' => array('title' => t('Add a new note to this '. $node_types[$node->type])),
      );
    }
  }
  return $links;
}

/*****************************************************************************************
 * Menu Callbacks
 ****************************************************************************************/

/**
 * 
 */
function notepad_summary($op = 'user') {
  global $user;
  
  if ($op == 'add') {
    drupal_goto('node/add/notepad');
  }
  
  switch ($op) {
    case 'user':
      // Headers
      $header = array(
        t('Priority'),
      );
    
      $sql = "SELECT n.nid, n.title, n.created, n.changed, m.priority FROM {node} n INNER JOIN {notepad_node_metadata} m ON n.vid=m.vid WHERE n.uid = %d AND m.personal = 1 ORDER BY m.priority, n.changed DESC";
      break;
      
    case 'content':
      drupal_set_title(t('My Content Notes'));

      // Headers
      $header = array(
        t('Type'),
        t('Notes'),
      );
      
      $sql = "SELECT COUNT(*) as note_count, n.nid, n.title, n.created, n.changed, n.uid, s.node_type FROM {node} n INNER JOIN {notepad_nodes} s ON n.nid=s.nid WHERE s.uid = %d GROUP BY n.nid ORDER BY n.changed DESC";
      break;
  }
  
  $header[] = t('Title');
  $header[] = t('Created');
  $header[] = t('Last Updated');
  
  $rows = array();
  $result = db_query($sql, $user->uid);
  while ($row = db_fetch_object($result)) {
    $data = array();
    if ($op == 'content') {
      $data[] = $row->node_type;
      $data[] = $row->note_count;
      $path = 'node/' . $row->nid .'/notepad';
    }
    else {
      $data[] = $row->priority;
      $path = 'node/' . $row->nid;
    }
    
    $data[] = array('data' => l(check_plain($row->title), $path), 'title' => check_plain($row->title), 'align' => 'left');
    $data[] = format_date($row->created, 'small');
    $data[] = t('!time ago', array('!time' => format_interval(time() - $row->changed)));

    $rows[] = $data;
  }
  
  if (!$rows) {
    $rows [] = array(array('data' => t('Sorry, no notes'), 'align' => 'center', 'colspan' => '4'));
  }

  // build table output
  $output = theme('table', $header, $rows, array('width' => '98%'));

  // Show the page
  print theme("page", $output);
}

/**
 *
 */
function notepad_node($nid, $op = '') {
  global $user;

  if ($op == 'add') {
    drupal_goto('node/add/notepad/'. $nid, 'destination=node/'. $nid .'/notepad');
  }

  // get all node types for display
  $node_types = node_get_types('names');
  
  // Set node title
  $node = node_load($nid);
  drupal_set_title($node->title);
  
  // Headers
  $header = array(
    t('Priority'),
    t('Title'),
    t('Created'),
    t('Last Updated'),
  );

  // Now a table of the results
  $rows = array();
  $result = db_query("SELECT n.nid, n.title, n.created, n.changed, s.node_type, m.priority FROM {node} n INNER JOIN {notepad_nodes} s ON n.nid=s.node_id INNER JOIN {notepad_node_metadata} m ON m.nid=s.node_id WHERE s.nid = %d AND n.uid = %d ORDER BY m.priority, n.created DESC", $nid, $user->uid);
  while ($row = db_fetch_object($result)) {
    $rows [] = array(
      $row->priority,
      array('data' => l(check_plain($row->title),'node/' . $row->nid), 'title' => check_plain($row->title), 'align' => 'left'),
      format_date($row->created, 'small'),
      t('!time ago', array('!time' => format_interval(time() - $row->changed))),
    );
  }

  if (!$rows) {
    $rows [] = array(array('data' => t('Sorry, no notes'), 'align' => 'center', 'colspan' => '4'));
  }

  // build table output
  $output = theme('table', $header, $rows, array('width' => '98%'));

  // Show the page
  print theme("page", $output);
}

/**
 *
 */
function notepad_display_node($node) {
  // Set the breadcrumb
  drupal_set_breadcrumb(array(l(t('Home'), ''), l(t('your notes'), 'notepad')));

  // Footer - priority info & actions link
  $output = '<br /><p><strong>' . t('Priority') . ':</strong> ' . $node->priority . '<br /><strong>' . t('Actions') . ':</strong> ' . l(t('Add New Note'), 'node/add/notepad') . '</p>';

  return $output;
}

/**
 *
 */
function notepad_settings() {
  $output = '';

  $node_types = node_get_types('names');
  unset($node_types['notepad']);
  $size = (count($node_types) <= 4 ? count($node_types) : 4);
  $form['notepad_node_types'] = array(
    '#type'          => 'select',
    '#title'         => t('Node types'),
    '#default_value' => variable_get('notepad_node_types', array()),
    '#options'       => $node_types,
    '#multiple'      => TRUE,
    '#size'          => $size,
    '#description'   => t("Select the node types that can have notes.")
  );

  return system_settings_form($form);
}
 
/*****************************************************************************************
 * form functions
 ****************************************************************************************/
 
 
 
/*****************************************************************************************
 * theme functions
 ****************************************************************************************/
 
function theme_notepad_block_content($rows) {
  $items = array();
  if (count($rows)) {
  
    // Add the links to the header
    $links[] = array('title' => t('My Notes'), 'href' => 'notepad');
    $links[] = array('title' => t('Add New Note'), 'href' => 'node/add/notepad');
    $output = theme('links', $links, array('class' => 'links inline'));
  
    foreach ($rows as $row) {
      $items[] = $row->priority . ' : ' . l($row->title, 'node/'. $row->nid, array('title' => check_plain($row->body)));
    }
    // Now render the list using theme_item_list
    $output .= theme('item_list', $items, NULL, 'ul');
  }
  return $output;
}
 
/*****************************************************************************************
 ****************************************************************************************/
 
