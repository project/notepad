The notepad module simply provides you with a way to info in the form of 'notes'.

Notes are listed in a block or in a main 'summary' page.

Notes are assigned a priority which influences the order they are listed.

You can have either 'personal notes', or 'content notes' which are notes made about other nodes.

Questions & comments: jon@professionalnerd.com or jondoesdrupal on drupal.org
